package bma.dao.repo;

import java.util.List;

import bma.model.Department;
import bma.model.Employee;

public interface DepartmentDaoRepo {

	Department save(Department department);

	void delete(Long id);

	Department updateName(Long id, String name);

	List<Department> findAll();

	Department findByName(String name);

	void updateEmployeeToDifferentDepartment(Department department, Department dept, Employee emp);

	void updateDepartmentNameByEmployee(String name, Employee emp);

	void deleteEmployeesByDepartment(Department dep);

	Integer totalNumberOfEmployeeByDepartment(Department dep);

	Department update(Department dep);

	Department getById(Long id);
}
