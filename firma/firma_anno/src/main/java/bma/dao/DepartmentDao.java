package bma.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bma.dao.repo.DepartmentDaoRepo;
import bma.model.Department;
import bma.model.Employee;

@Service("departmentDao")
@Transactional
public class DepartmentDao {

	private Class<Department> dept;

	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional
	public void delete(Department dep) {
		entityManager.remove(dep);
	}

	@Transactional
	public
	void delete(Long id) {
		Department department = getById(id);
		delete(department);
	}

	@Transactional
	public
	void deleteEmployeesByDepartment(Department dep) {

	}

	@Transactional
	public
	List<Department> findAll() {
		return entityManager.createQuery("from " + Department.class.getName()).getResultList();
	}

	@Transactional
	public
	Department findByName(String name) {
		return (Department) entityManager
				.createQuery(("select department from department department where department.name =:name"
						+ Department.class.getName()))
				.getSingleResult();
	}

	@Transactional
	public
	Department getById(Long id) {
		return entityManager.find(Department.class, id);
	}

	@Transactional
	public void save(Department department) {
		entityManager.persist(department);
	}

	@Transactional
	public Integer totalNumberOfEmployeeByDepartment(Department dep) {
		return entityManager.find(Department.class, dep).getEmployees().size();
	}

	@Transactional
	public void update(Department dep) {
		entityManager.merge(dep);
	}

	// void updateDepartmentNameByEmployee(String name, Employee emp);
	@Transactional
	public void updateEmployeeToDifferentDepartment(Department department, Department deptTo, Employee emp) {
		entityManager.find(Department.class, deptTo).setEmployees((List<Employee>) emp);
	}

	@Transactional
	public void updateName(Long id, String name) {
		entityManager.find(Department.class, id).setName(name);
	}

}
