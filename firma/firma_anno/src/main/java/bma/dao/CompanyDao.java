package bma.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bma.dao.repo.CompanyDaoRepo;
import bma.model.Company;

@Service("companyDao")
@Transactional
public class CompanyDao implements CompanyDaoRepo {

	private Class<Company> comp;

	@PersistenceContext
	protected EntityManager entityManager;

	@Override
	@Transactional
	public void delete(Company company) {
		entityManager.remove(company);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Company company = getById(id);
		delete(company);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Company> findAll() {
		return entityManager.createQuery("from " + Company.class.getName()).getResultList();
	}

	@Override
	@Transactional
	public Company findOne(long id) {
		return entityManager.find(Company.class, id);
	}

	@Override
	@Transactional
	public List<Company> getByCity(String city) {
		return entityManager
				.createQuery(
						("select company from Company company where company.city =:city" + Company.class.getName()))
				.getResultList();
	}

	@Override
	@Transactional
	public Company getById(Long id) {

		return entityManager.find(Company.class, id);
	}

	@Override
	@Transactional
	public void save(Company company) {
		entityManager.persist(company);
	}

	@Override
	@Transactional
	public void update(Company company) {
		entityManager.merge(company);
	}

}
