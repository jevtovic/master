package bma.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bma.dao.CompanyDao;
import bma.model.Company;
import bma.repository.CompanyRepository;
import bma.service.CompanyService;

@Service("companyService")
@Transactional
public class CompanyServiceImpl implements CompanyService  {

	@Autowired
	private CompanyRepository companyRepository;

	public List<Company> findAll() {
		return companyRepository.findAll();
	}

	@Override
	public void save(Company company) {
		 companyRepository.save(company);
	}

	@Override
	public void delete(Long id) {
		companyRepository.delete(id);
	}

	@Override
	public List<Company> getByCity(String city) {
		return companyRepository.getByCity(city);
	}

	@Override
	public Company getById(Long id) {
		return companyRepository.getById(id);
	}

	@Override
	public void update(Company com) {
		 companyRepository.update(com);
	}

	@Override
	public void delete(Company company) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Company findOne(long id) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public void delete(Company company) {
//		companyRepository.delete(company);
//		
//	}
//
//	@Override
//	public Company findOne(long id) {
//		
//		return companyRepository.findOne(id);
//	}

}
