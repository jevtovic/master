package bma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bma.model.Department;
import bma.model.Employee;
import bma.repository.DepartmentRepository;
import bma.service.DepartmentService;

@Service("departmentService")
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	@Override
	public void save(Department department) {
		 departmentRepository.save(department);
	}

	@Override
	public void delete(Long id) {
		departmentRepository.delete(id);
	}

	@Override
	public void updateName(Long id, String name) {
		 departmentRepository.updateName(id, name);
	}

	@Override
	public Department findByName(String name) {
		return departmentRepository.findByName(name);
	}

	@Override
	public List<Department> findAll() {
		return departmentRepository.findAll();
	}

	@Override
	public void updateEmployeeToDifferentDepartment(Department department, Department dept, Employee emp) {
		departmentRepository.updateEmployeeToDifferentDepartment(department, dept, emp);
	}

//	@Override
//	public void updateDepartmentNameByEmployee(String name, Employee emp) {
//		departmentRepository.updateDepartmentNameByEmployee(name, emp);
//
//	}

	@Override
	public void deleteEmployeesByDepartment(Department dep) {
		departmentRepository.deleteEmployeesByDepartment(dep);

	}

	@Override
	public Integer totalNumberOfEmployeeByDepartment(Department dep) {
		departmentRepository.totalNumberOfEmployeeByDepartment(dep);
		return null;
	}

	@Override
	public void update(Department dep) {
		departmentRepository.update(dep);
	}

	@Override
	public Department getById(Long id) {

		return departmentRepository.getById(id);
	}

}
