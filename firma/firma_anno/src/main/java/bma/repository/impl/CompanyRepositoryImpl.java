package bma.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import bma.dao.CompanyDao;
import bma.model.Company;
import bma.repository.CompanyRepository;

@Repository("companyRepository")
public class CompanyRepositoryImpl implements CompanyRepository {

//	private Long nextId = 0L;
//	private List<Company> companies = new ArrayList<>();
//
//	Company company1 = new Company(nextId++, "Bright Marbles", "Srbija", "Novi Sad", "Danila Kisa");
//	Company company2 = new Company(nextId++, "Levi 9", "Srbija", "Novi Sad", "Trg Marije Trandafil");
//	Company company3 = new Company(nextId++, "Namics", "Srbija", "Beograd", "Bulevar Kralja Aleksandra");
//	{
//
//		companies.add(company1);
//		companies.add(company2);
//		companies.add(company3);
//
//	}
	@Autowired
	CompanyDao companyDao;

	@Override
	public List<Company> findAll() {
//		System.out.println("prikazujemo sve kompanije: " + companies);
		return companyDao.findAll();
	}

	@Override
	public void save(Company company) {
		
		companyDao.save(company);
	}

	@Override
	public void delete(Long id) {
		companyDao.delete(id);
		
	}

	@Override
	public List<Company> getByCity(String city) {
		return companyDao.getByCity(city);
	}

	@Override
	public Company getById(Long id) {
		return companyDao.findOne(id);
		
	}

	@Override
	public Company update(Company com) {
		 companyDao.update(com);
		return com;
	}
	
	
	
	

//	@Override
//	public Company save(Company company) {
//		Company retVal = null;
//		company.setId(nextId++);
//		companies.add(company);
//		retVal = company;
//		System.out.println("sacuvana kompanija" + retVal);
//		return retVal;
//	}
//
//	@Override
//	public void delete(Long id) {
//		Company company = null;
//		for (int i = 0; i < companies.size(); i++) {
//			if (i == id)
//				company = companies.get(i);
//			companies.remove(company);
//		}
//		System.out.println("obrisana kompanija: " + company);
//	}
//
//	@Override
//	public List<Company> getByCity(String city) {
//		List<Company> retVal = new ArrayList<>();
//
//		for (Company company : companies) {
//			if (company.getCity().equals(city)) {
//
//				retVal.add(company);
//			}
//		}
//		System.out.println("trazene kompanije su: " + retVal);
//		return retVal;
//
//	}
//
//	@Override
//	public Company getById(Long id) {
//		Company company = null;
//		for (int i = 0; i < companies.size(); i++) {
//			if (i == id)
//				company = companies.get(i);
//
//		}
//		System.out.println("pronadjena kompanija: " + company);
//		return company;
//	}
//
//	@Override
//	public Company update(Company com) {
//		Company edited = null;
//		int editedIndex = 0;
//
//		for (int i = 0; i < companies.size(); i++) {
//
//			if (companies.get(i).getId().equals(com.getId())) {
//				editedIndex = i;
//				edited = com;
//			}
//		}
//		if (edited != null) {
//			companies.remove(editedIndex);
//			companies.add(edited);
//		}
//		System.out.println("izmenjen departman" + edited);
//		return edited;
//	}
}
