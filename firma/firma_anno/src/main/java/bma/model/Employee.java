package bma.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="employee")
public class Employee {

	@Id
	@GeneratedValue
	private Long id;
	@Column
	private String fullName;
	@Column
	private String phone;
	@Column
	private String email;
	@Column
	private BigDecimal salary;
	@ManyToOne
	private Department department;

	public Employee() {

	}

	public Employee(Long id, String fullName, String phone, String email, BigDecimal salary) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.phone = phone;
		this.email = email;
		this.salary = salary;
	}

	public Employee(Long id, String fullName, String phone, String email, BigDecimal salary, Department department) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.phone = phone;
		this.email = email;
		this.salary = salary;
		this.department = department;
	}

	public Employee(String fullName, String phone, String email, BigDecimal salary) {
		super();
		this.fullName = fullName;
		this.phone = phone;
		this.email = email;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Zaposleni sa Imenom i prezimenom" + fullName + ",  brojem telefona" + phone + ", email adresom " + email
				+ ", prima platu u iznosu od: " + salary + ", i pripada departmanu: " + department + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}
