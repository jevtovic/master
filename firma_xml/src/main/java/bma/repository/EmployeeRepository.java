package bma.repository;

import java.util.List;

import bma.model.Department;
import bma.model.Employee;

public interface EmployeeRepository {

	Employee save(Employee employee);
    Employee delete(Long id);    
    List<Employee> findAll();   
    Employee getEmployeeWithHighestSalary();
    
	
}