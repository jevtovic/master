package bma.repository.impl;

import java.util.ArrayList;
import java.util.List;

import bma.model.Department;
import bma.model.Employee;
import bma.repository.EmployeeRepository;

public class EmployeeRepositoryImpl implements EmployeeRepository {

	private Long nextId = 0L;
	private List<Employee> employees = new ArrayList<>();

	Employee employee1 = new Employee();
	{
		employee1.setId(nextId++);
		employee1.setFullName("Gospodin direktor");
		employee1.setPhone("0641233333");
		employee1.setEmail("NoviSad@mail");
		employee1.setSalary(400000);
		employees.add(employee1);
	}

	Employee employee2 = new Employee();
	{
		employee2.setId(nextId++);
		employee2.setFullName("Gospodin radnik");
		employee2.setPhone("064000000");
		employee2.setEmail("mile@mail");
		employee2.setSalary(80000);
		employees.add(employee2);
	}

	Employee employee3 = new Employee();
	{
		employee3.setId(nextId++);
		employee3.setFullName("Gospodja sekretarica");
		employee3.setPhone("064000000");
		employee3.setEmail("mileva@mail");
		employee3.setSalary(80000);
		employees.add(employee3);
	}

	@Override
	public Employee save(Employee employee) {
		Employee retVal = null;
		employee.setId(nextId);
		employees.add(employee);
		retVal = employee;
		return retVal;
	}

	@Override
	public Employee delete(Long id) {
		Employee emp = null;
		for (int i = 0; i < employees.size(); i++) {
			if (i == id) {
				emp = employees.get(i);
				employees.remove(emp);
			}
		}
		return emp;
	}

	@Override
	public Employee getEmployeeWithHighestSalary() {
		Integer temp = employees.get(0).getSalary();
		Employee empl = null;
		for (Employee employee : employees) {
			if (employee.getSalary() >= temp) {
				empl = employee;
				temp = employee.getSalary();
			}
		}
		return empl;
	}

	@Override
	public List<Employee> findAll() {
		return employees;
	}

}
