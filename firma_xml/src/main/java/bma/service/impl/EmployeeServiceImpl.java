package bma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bma.model.Department;
import bma.model.Employee;
import bma.repository.EmployeeRepository;
import bma.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepository employeRepository;

	
	
	public EmployeeRepository getEmployeRepository() {
		return employeRepository;
	}

	public void setEmployeRepository(EmployeeRepository employeRepository) {
		this.employeRepository = employeRepository;
	}

	public EmployeeServiceImpl(EmployeeRepository employeRepository) {
		super();
		this.employeRepository = employeRepository;
	}

	public EmployeeServiceImpl() {
		super();
	}

	@Override
	public Employee save(Employee employee) {
		return employeRepository.save(employee);
	}

	@Override
	public Employee delete(Long id) {
		return employeRepository.delete(id);
	}

	@Override
	public void updateDepartmentNameByEmployee(String name, Long employeeId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteEmployeesByDepartment(Long departmentId) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Employee> findAll() {
		return employeRepository.findAll();
	}

	@Override
	public Integer totalNumberOfEmployeeByDepartment(Long departmentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee getEmployeeWithHighestSalary() {
		return employeRepository.getEmployeeWithHighestSalary();
	}

	@Override
	public void updateEmployeeToDifferentDepartment(Department department) {
		// TODO Auto-generated method stub

	}

}
