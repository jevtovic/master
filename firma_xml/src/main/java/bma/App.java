package bma;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import bma.model.Company;
import bma.model.Department;
import bma.model.Employee;
import bma.service.CompanyService;
import bma.service.DepartmentService;
import bma.service.EmployeeService;

public class App {

	public static void main(String[] args) {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");

		CompanyService service = appContext.getBean("companyService", CompanyService.class);
		DepartmentService dService = appContext.getBean("departmentService", DepartmentService.class);
		EmployeeService eService = appContext.getBean("employeeService", EmployeeService.class);

		Company c1 = new Company("Naovis", "Srbija", "Novi Sad", "negde u Novome Sadu");		
		service.save(c1);
		System.out.println(service.findAll().get(0).getName());
		System.out.println(service.findAll().get(3).getName());
		System.out.println(service.findAll().size());
		service.delete(1L);
		System.out.println(service.findAll().size());
		System.out.println(service.findAll());
		service.getByCity("Beograd");
		
		dService.findAll();
		Department department = new Department("lezilebovici", 1L);
		dService.save(department);
		dService.findAll();
		System.out.println(dService.findAll());
		dService.delete(3L);
		dService.findByName("R&D");
		System.out.println(dService.findByName("R&D"));
		dService.updateName(2L, "pokusaj");
		System.out.println(dService.findAll());
		
		eService.findAll();
		System.out.println(eService.findAll());
		Employee emp = new Employee("Mirko", "063896523", "mirko@gmail", 90000);
		eService.save(emp);
		System.out.println(eService.findAll());
		System.out.println(eService.getEmployeeWithHighestSalary());
		System.out.println(eService.delete(3L));
		
	}
}