package bma.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

public class Company {

	
	private Long id;

	private String name;

	private String country;

	private String city;

	private String address;

	private List<Department> departments = new ArrayList<>();

	public Company() {

	}

	public Company(Long id, String name, String country, String city, String address, List<Department> departments) {
		super();
		this.id = id;
		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
		this.departments = departments;
	}
	
	

	public Company(String name, String country, String city, String address) {
		super();
		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
	}

	public Company(Long id, String name, String country, String city, String address) {
		super();
		this.id = id;
		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + ", country=" + country + ", city=" + city + ", address="
				+ address + ", departments=" + departments + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

}
