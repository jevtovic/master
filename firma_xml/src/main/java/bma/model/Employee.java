package bma.model;

public class Employee {

	private Long id;
	
	private String fullName;
	
	private String phone;
	
	private String email;
	
	private Integer salary;
	
	private Department department;

	public Employee() {
		
	}

	public Employee(Long id, String fullName, String phone, String email, Integer salary, Department department) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.phone = phone;
		this.email = email;
		this.salary = salary;
		this.department = department;
	}

	
	
	public Employee(String fullName, String phone, String email, Integer salary) {
		super();
		this.fullName = fullName;
		this.phone = phone;
		this.email = email;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", fullName=" + fullName + ", phone=" + phone + ", email=" + email + ", salary="
				+ salary + ", department=" + department + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
	
	
	
}
