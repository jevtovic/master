package bma.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import bma.model.Department;
import bma.model.Employee;
import bma.repository.DepartmentRepository;

@Repository("departmentRepository")
public class DepartmentRepositoryImpl implements DepartmentRepository {

	private Long nextId = 0L;
	private List<Department> departments = new ArrayList<>();
	private List<Employee> employees = new ArrayList<>();

	Department department1 = new Department(nextId++, "R&D", 3L, employees);
	{
		departments.add(department1);
	}

	Department department2 = new Department(nextId++, "IT", 4L);
	{
		departments.add(department2);
	}

	Department department3 = new Department(nextId++, "Marketing", 2L);
	{
		departments.add(department3);
	}

	@Override
	public Department save(Department department) {
		Department retVal = null;
		department.setId(nextId++);
		departments.add(department);
		retVal = department;
		System.out.println("sacuvan departman" + retVal);
		return retVal;
	}

	@Override
	public Department delete(Long id) {
		Department department = null;
		for (int i = 0; i < departments.size(); i++) {
			if (i == id)
				department = departments.get(i);
			departments.remove(department);
		}
		System.out.println("obrisan departman" + department);
		return department;
	}

	@Override
	public Department updateName(Long id, String name) {
		Department retVal = null;
		for (Department department : departments) {
			if (department.getId() == id) {
				department.setName(name);
				retVal = department;
			}
		}
		System.out.println("izmenjen departman" + retVal);
		return retVal;
	}

	@Override
	public List<Department> findAll() {
		System.out.println("prikazujemo listu svih departmana: " + departments);
		return departments;
	}

	@Override
	public Department findByName(String name) {
		Department retVal = null;
		for (Department department : departments) {
			if (department.getName().equals(name)) {
				retVal = department;
			}
		}
		System.out.println("pronadjen odgovarajuci departman:" + retVal);
		return retVal;
	}

	@Override
	public void updateEmployeeToDifferentDepartment(Department izDep, Department uDep, Employee emp) {
		
		for (Employee employ : izDep.getEmployees()) {
			if (employ.equals(emp)) {
				uDep.setEmployee(emp);
				izDep.getEmployees().remove(emp);
			}
		}
		System.out.println("zaposleni "+emp+" prebacen iz departmana u drugi");
	}

	@Override
	public void updateDepartmentNameByEmployee(String name, Employee emp) {
		for (Employee employ : employees) {
			if(employ.equals(emp)) {
				emp.getDepartment().setName(name);
			}
		}
		System.out.println("promenjen naziv departmana u: "+name);
	}

	@Override
	public void deleteEmployeesByDepartment(Department dep) {
		for (Employee empl : dep.getEmployees()) {
			delete(empl.getId());
		}
		
	}

	@Override
	public Integer totalNumberOfEmployeeByDepartment(Department dep) {
		int sum=dep.getEmployees().size();
		return sum;
	}

}
