package bma.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import bma.model.Company;
import bma.repository.CompanyRepository;

@Repository("companyRepository")
public class CompanyRepositoryImpl implements CompanyRepository {

	private Long nextId = 0L;
	private List<Company> companies = new ArrayList<>();

	Company company1 = new Company(nextId++, "Bright Marbles", "Srbija", "Novi Sad", "Danila Kisa" );
	Company company2 = new Company(nextId++, "Levi 9", "Srbija", "Novi Sad", "Trg Marije Trandafil" );
	Company company3 = new Company(nextId++, "Namics", "Srbija", "Beograd", "Bulevar Kralja Aleksandra" );
	{
	
	companies.add(company1);
	companies.add(company2);
	companies.add(company3);

	}
	

	@Override
	public List<Company> findAll() {
		System.out.println("prikazujemo sve kompanije: " + companies);
		return companies;
	}

	@Override
	public Company save(Company company) {
		Company retVal = null;
		company.setId(nextId);
		companies.add(company);
		retVal = company;
		System.out.println("sacuvana kompanija" + retVal);
		return retVal;
	}

	@Override
	public Company delete(Long id) {
		Company company = null;
		for (int i = 0; i < companies.size(); i++) {
			if (i == id)
				company = companies.get(i);
			companies.remove(company);
		}
		System.out.println("obrisana kompanija: " + company);
		return company;
	}

	@Override
	public List<Company> getByCity(String city) {
		List<Company> retVal = new ArrayList<>();

		for (Company company : companies) {
			if (company.getCity().equals(city)) {

				retVal.add(company);
			}
		}
		System.out.println("trazene kompanije su: " + retVal);
		return retVal;

	}

}
