package bma.repository;

import java.util.List;

import bma.model.Company;

public interface CompanyRepository {

	List<Company> findAll();
    Company save(Company company);
    Company delete(Long id);
    List<Company> getByCity(String city);
	
}