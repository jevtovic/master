package bma.model;

import java.util.ArrayList;
import java.util.List;

public class Department {

	private Long id;

	private String name;

	private Long noEmployees;
	
	private Employee employee;

	private List<Employee> employees = new ArrayList<>();

	private Company company;

	public Department() {

	}

	public Department(Long id, String name, Long noEmployees, List<Employee> employees, Company company) {
		super();
		this.id = id;
		this.name = name;
		this.noEmployees = noEmployees;
		this.employees = employees;
		this.company = company;
	}
	
	

	public Department(Long id, String name, Long noEmployees, List<Employee> employees) {
		super();
		this.id = id;
		this.name = name;
		this.noEmployees = noEmployees;
		this.employees = employees;
	}

	public Department(Long id, String name, Long noEmployees) {
		this.id=id;
		this.name = name;
		this.noEmployees = noEmployees;
	}
	
	

	public Department(Long id, String name, Long noEmployees, Employee employee) {
		super();
		this.id = id;
		this.name = name;
		this.noEmployees = noEmployees;
		this.employee = employee;
	}

	public Department(String name, Long noEmployees) {
		super();
		this.name = name;
		this.noEmployees = noEmployees;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", noEmployees=" + noEmployees + ", employees=" + employees
				+ ", company=" + company + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNoEmployees() {
		return noEmployees;
	}

	public void setNoEmployees(Long noEmployees) {
		this.noEmployees = noEmployees;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	
}
