package bma;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import bma.repository.CompanyRepository;
import bma.repository.DepartmentRepository;
import bma.repository.EmployeeRepository;
import bma.repository.impl.CompanyRepositoryImpl;
import bma.repository.impl.DepartmentRepositoryImpl;
import bma.repository.impl.EmployeeRepositoryImpl;
import bma.service.CompanyService;
import bma.service.DepartmentService;
import bma.service.EmployeeService;
import bma.service.impl.CompanyServiceImpl;
import bma.service.impl.DepartmentServiceImpl;
import bma.service.impl.EmployeeServiceImpl;

@Configuration
@ComponentScan({ "bma" })
// @PropertySource("app.properties")
public class AppConfig {

	@Bean(name = "companyService")
	public CompanyService getCompanyService() {

		CompanyServiceImpl service = new CompanyServiceImpl(getCompanyRepository());
		return service;
	}

	@Bean(name = "companyRepository")
	public CompanyRepository getCompanyRepository() {
		return new CompanyRepositoryImpl();
	}

	@Bean(name = "departmentService")
	public DepartmentService getDepartmentService() {

		DepartmentServiceImpl service = new DepartmentServiceImpl(getDepartmentRepository());
		return service;
	}

	@Bean(name = "departmentRepository")
	public DepartmentRepository getDepartmentRepository() {
		return new DepartmentRepositoryImpl();
	}

	@Bean(name = "employeeService")
	public EmployeeService getEmployeeService() {

		EmployeeServiceImpl service = new EmployeeServiceImpl(getEmployeeRepository());
		return service;
	}

	@Bean(name = "employeeRepository")
	public EmployeeRepository getEmployeeRepository() {
		return new EmployeeRepositoryImpl();
	}
}
