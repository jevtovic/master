package bma.service;

import java.util.List;

import bma.model.Company;

public interface CompanyService {

	List<Company> findAll();

	Company save(Company company);

	Company delete(Long id);

	List<Company> getByCity(String city);
}