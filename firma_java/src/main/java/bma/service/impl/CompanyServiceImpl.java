package bma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import bma.model.Company;
import bma.repository.CompanyRepository;
import bma.service.CompanyService;


@Service("companyService")
@Scope
public class CompanyServiceImpl implements CompanyService {

	private CompanyRepository companyRepository;

	
	
	public CompanyRepository getCompanyRepository() {
		return companyRepository;
	}

	public void setCompanyRepository(CompanyRepository companyRepository) {
		this.companyRepository = companyRepository;
	}

	public CompanyServiceImpl() {
		super();
	}

	public CompanyServiceImpl(CompanyRepository companyRepository) {
		this.companyRepository = companyRepository;
	}

	@Override
	public List<Company> findAll() {
		return companyRepository.findAll();
	}

	@Override
	public Company save(Company company) {
		return companyRepository.save(company);
	}

	@Override
	public Company delete(Long id) {
		return companyRepository.delete(id);
	}

	@Override
	public List<Company> getByCity(String city) {
		return companyRepository.getByCity(city);
	}

}
