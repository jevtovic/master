package bma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import bma.model.Department;
import bma.repository.DepartmentRepository;
import bma.service.DepartmentService;

@Service("departmentService")
@Scope
public class DepartmentServiceImpl implements DepartmentService {

	private DepartmentRepository departmentRepository;

	public DepartmentRepository getDepartmentRepository() {
		return departmentRepository;
	}

	public void setDepartmentRepository(DepartmentRepository departmentRepository) {
		this.departmentRepository = departmentRepository;
	}

	public DepartmentServiceImpl() {
		super();
	}

	public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
		super();
		this.departmentRepository = departmentRepository;
	}

	@Override
	public Department save(Department department) {
		return departmentRepository.save(department);
	}

	@Override
	public Department delete(Long id) {
		return departmentRepository.delete(id);
	}

	@Override
	public Department updateName(Long id, String name) {
		return departmentRepository.updateName(id, name);
	}

	@Override
	public Department findByName(String name) {
		return departmentRepository.findByName(name);
	}

	@Override
	public List<Department> findAll() {
		return departmentRepository.findAll();
	}

}
