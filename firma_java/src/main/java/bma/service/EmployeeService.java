package bma.service;

import java.util.List;

import bma.model.Department;
import bma.model.Employee;

public interface EmployeeService {

	Employee save(Employee employee);

	Employee delete(Long id);

	void updateDepartmentNameByEmployee(String name, Long employeeId);

	void deleteEmployeesByDepartment(Long departmentId);

	List<Employee> findAll();

	Integer totalNumberOfEmployeeByDepartment(Long departmentId);

	Employee getEmployeeWithHighestSalary();

	void updateEmployeeToDifferentDepartment(Department department);
}