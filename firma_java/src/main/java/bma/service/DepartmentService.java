package bma.service;

import java.util.List;

import bma.model.Department;

public interface DepartmentService {

	
	Department save(Department department);
    Department delete(Long id);
    Department updateName(Long id, String name);
    List<Department> findAll();
    Department findByName(String name);
}