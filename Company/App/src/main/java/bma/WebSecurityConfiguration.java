package bma;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.stereotype.Component;

import bma.service.impl.DetailsService;

@Component
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	DetailsService detailsService;
	
//	@Autowired
//	private EmployeeServiceImpl emplSerImpl;
	
	

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(detailsService); //.passwordEncoder(Employee.PASSWORD_ENCODER);
	}
	
//	  @Autowired
//	    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
//	        auth.inMemoryAuthentication().withUser("pera").password("password").roles("ADMIN");
//	        auth.inMemoryAuthentication().withUser("root").password("root1").roles("USER");
//	        auth.inMemoryAuthentication().withUser("dragance").password("password").roles("ADMIN","USER");
//	    }

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        
		http.csrf().disable();
		http.httpBasic().and().authorizeRequests().antMatchers("api/**").permitAll();	
		
		
//		http.httpBasic().and().authorizeRequests().antMatchers("api/company/**").permitAll()
//      .antMatchers("api/department/**").hasAnyRole("ADMIN", "USER").antMatchers("api/employee/**")
//      .hasRole("ADMIN");
		
		
		
//		http.authorizeRequests()
//                .anyRequest().authenticated()
////                .antMatchers("api/employee/**").hasRole("ADMIN")
//                .and()
////                .antMatchers("api/employee").permitAll() //("ADMIN", "USER")
////                .anyRequest().authenticated()
////                .and()
//                .httpBasic()
//                .and()
//                .withUser("pera").password("password")
//                .authorities("ADMIN")
//                .csrf().disable();
    }
	@Bean
    public static NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }
}
