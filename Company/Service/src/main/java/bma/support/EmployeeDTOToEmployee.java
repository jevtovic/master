package bma.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bma.model.Employee;
import bma.service.EmployeeService;
import bma.web.dto.EmployeeDTO;

@Component
public class EmployeeDTOToEmployee implements Converter<EmployeeDTO, Employee> {

	@Autowired
	private EmployeeService employeeService;

	@Override
	public Employee convert(EmployeeDTO source) {
		Employee employee = null;

		if (source.getId() == null) {
			employee = new Employee();

		} else {
			employee = employeeService.getById(source.getId());
		}
		employee.setFullName(source.getFullName());
		employee.setPhone(source.getPhone());
		employee.setEmail(source.getEmail());
		employee.setSalary(source.getSalary());

		return employee;
	}


	
}
