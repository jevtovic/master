package bma.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bma.model.Department;
import bma.web.dto.DepartmentDTO;

@Component
public class DepartmentToDepartmentDTO implements Converter<Department, DepartmentDTO> {

	@Override
	public DepartmentDTO convert(Department source) {
		DepartmentDTO dto = new DepartmentDTO();

		dto.setId(source.getId());
		dto.setName(source.getName());
		dto.setNoEmployees(source.getNoEmployees());

		return dto;
	}

	public List<DepartmentDTO> convert(List<Department> departments) {
		List<DepartmentDTO> ret = new ArrayList<>();

		for (Department d : departments) {
			ret.add(convert(d));
		}

		return ret;
	}

}
