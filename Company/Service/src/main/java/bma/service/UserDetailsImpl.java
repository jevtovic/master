package bma.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import bma.model.Employee;
import bma.model.Role;

public class UserDetailsImpl implements UserDetails {

	private Employee employee;
	
	public UserDetailsImpl(Employee employee) {
		this.employee= employee;
	}
	
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		 Collection<GrantedAuthority> authorities = new ArrayList<>();
		 List<Role> roles = employee.getRoles();
         for (Role role:roles) {
             authorities.add(new SimpleGrantedAuthority(role.getName()));
         }
        return authorities;
    }

	@Override
	public String getPassword() {
		 return employee.getPassword();
	}

	@Override
	public String getUsername() {
		return employee.getPassword();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
