package bma.repository;

import java.math.BigDecimal;
import java.util.List;

import bma.model.Employee;

public interface EmployeeRepository {

	void save(Employee employee);

	void delete(Long id);

	List<Employee> findAll();

	BigDecimal getEmployeeWithHighestSalary();
	
	Employee getById(Long id);
	
	void update (Employee emp);

}