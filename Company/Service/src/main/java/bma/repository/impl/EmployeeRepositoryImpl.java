package bma.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import bma.dao.EmployeeDao;
import bma.model.Employee;
import bma.repository.EmployeeRepository;

@Repository("employeeRepository")
public class EmployeeRepositoryImpl implements EmployeeRepository {

	// private Long nextId = 0L;
	// public List<Employee> employees = new ArrayList<>();
	//
	// Employee employee1 = new Employee();
	// {
	// employee1.setId(nextId++);
	// employee1.setFullName("Gospodin direktor");
	// employee1.setPhone("0641233333");
	// employee1.setEmail("NoviSad@mail");
	// employee1.setSalary(new BigDecimal(400000.00));
	// employees.add(employee1);
	// }
	//
	// Employee employee2 = new Employee();
	// {
	// employee2.setId(nextId++);
	// employee2.setFullName("Gospodin radnik");
	// employee2.setPhone("064000000");
	// employee2.setEmail("mile@mail");
	// employee2.setSalary(new BigDecimal(90000.00));
	// employees.add(employee2);
	// }
	//
	// Employee employee3 = new Employee();
	// {
	// employee3.setId(nextId++);
	// employee3.setFullName("Gospodja sekretarica");
	// employee3.setPhone("064000000");
	// employee3.setEmail("mileva@mail");
	// employee3.setSalary(new BigDecimal(80000.00));
	// employees.add(employee3);
	// }

	@Autowired
	EmployeeDao employeeDao;

	@Override
	public void delete(Long id) {
		employeeDao.delete(id);
	}

	@Override
	public List<Employee> findAll() {
		return employeeDao.findAll();
	}

	@Override
	public Employee getById(Long id) {
		return employeeDao.getById(id);
	}

	@Override
	public BigDecimal getEmployeeWithHighestSalary() {
		return employeeDao.getEmployeeWithHighestSalary();
	}

	@Override
	public void save(Employee employee) {
		employeeDao.save(employee);
		;
	}

	@Override
	public void update(Employee emp) {
		employeeDao.update(emp);
	}

}
