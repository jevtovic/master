package bma.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bma.model.Employee;

@Service("employeeDao")
@Transactional
public class EmployeeDao {

	private Class<Employee> empl;

	@PersistenceContext
	protected EntityManager entityManager;

	@Transactional
	public void delete(Employee employee) {
		entityManager.remove(employee);
	}

	@Transactional
	public void delete(Long id) {
		Employee empl = getById(id);
		delete(empl);
	}

	@Transactional
	public List<Employee> findAll() {
		return entityManager.createQuery("from" + Employee.class.getName()).getResultList();
	}

	@Transactional
	public Employee getById(Long id) {
		return entityManager.find(Employee.class, id);
	}

	@Transactional
	public BigDecimal getEmployeeWithHighestSalary() {
		return (BigDecimal) entityManager
				.createQuery("select max(salary) from Employee employee " + Employee.class.getName()).getSingleResult();
	}

	@Transactional
	public void save(Employee employee) {
		entityManager.persist(employee);
	}

	@Transactional
	public void update(Employee emp) {
		entityManager.merge(emp);
	}

}
