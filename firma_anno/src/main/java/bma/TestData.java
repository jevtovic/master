package bma;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import bma.model.Company;
import bma.model.Department;
import bma.model.Employee;
import bma.service.CompanyService;
import bma.service.DepartmentService;
import bma.service.EmployeeService;

// Inicijalni podaci za bazu
@Component
public class TestData {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private EmployeeService employeeService;

	@PostConstruct
	public void init() {
		Company company1 = new Company("BrightMarbles", "Serbia", "Novi Sad", "Danila Kisa 3");
		Company company2 = new Company("Synechron", "Serbia", "Novi Sad", "Vojvodjanskih Brigada 28");
		Company company3 = new Company("Coing", "Serbia", "Beograd", "Laze Kostica 17");

		companyService.save(company1);
		companyService.save(company2);
		companyService.save(company3);

		Department department1 = new Department("Marketing", 5L);
		Department department2 = new Department("Kontroling", 6L);
		Department department3 = new Department("Finansije", 4L);

		
		company1.getDepartments().add(department1);
		company1.getDepartments().add(department2);
		company1.getDepartments().add(department3);

		departmentService.save(department1);
		departmentService.save(department2);
		departmentService.save(department3);

		Employee employee1 = new Employee("Petar Petrovic", "065/515-56-98", "petar@gmail.com", new BigDecimal(80000));
		Employee employee2 = new Employee("Petar Jovic", "065/515-56-99", "petarj@gmail.com", new BigDecimal(100000));
		Employee employee3 = new Employee("Dragan Dragic", "065/515-57-98", "dragan@gmail.com", new BigDecimal(90000));

		department1.getEmployees().add(employee1);
		department1.getEmployees().add(employee2);
		department2.getEmployees().add(employee3);

		employeeService.save(employee1);
		employeeService.save(employee2);
		employeeService.save(employee3);
	}
}
