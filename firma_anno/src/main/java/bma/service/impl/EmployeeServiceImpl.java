package bma.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bma.model.Employee;
import bma.repository.EmployeeRepository;
import bma.service.EmployeeService;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeRepository;

	@Override
	public void save(Employee employee) {
		employeRepository.save(employee);
	}

	@Override
	public void delete(Long id) {
		employeRepository.delete(id);
	}

	@Override
	public List<Employee> findAll() {
		return employeRepository.findAll();
	}

	@Override
	public BigDecimal getEmployeeWithHighestSalary() {
		return employeRepository.getEmployeeWithHighestSalary();
	}

	@Override
	public void update(Employee emp) {
		employeRepository.update(emp);
	}

	@Override
	public Employee getById(Long id) {

		return employeRepository.getById(id);
	}
}
