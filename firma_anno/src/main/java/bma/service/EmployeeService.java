package bma.service;

import java.math.BigDecimal;
import java.util.List;

import bma.model.Employee;

public interface EmployeeService {

	void save(Employee employee);

	void delete(Long id);

	List<Employee> findAll();

	BigDecimal getEmployeeWithHighestSalary();

	Employee getById(Long id);

	void update( Employee emp);

}