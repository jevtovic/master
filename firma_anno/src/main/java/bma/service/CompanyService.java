package bma.service;

import java.util.List;

import bma.model.Company;

public interface CompanyService {

	public void delete(Company company);

	void delete(Long id);

	public List<Company> findAll();

	public Company findOne(long id);

	List<Company> getByCity(String city);

	public Company getById(Long id);

	void save(Company company);

	void update(Company company);
}