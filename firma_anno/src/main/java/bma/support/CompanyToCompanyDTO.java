package bma.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bma.model.Company;
import bma.web.dto.CompanyDTO;

@Component
public class CompanyToCompanyDTO implements Converter<Company, CompanyDTO> {

	@Override
	public CompanyDTO convert(Company source) {
		CompanyDTO dto = new CompanyDTO();

		dto.setId(source.getId());
		dto.setName(source.getName());
		dto.setCountry(source.getCountry());
		dto.setCity(source.getCity());
		dto.setAddress(source.getAddress());

		return dto;
	}

	public List<CompanyDTO> convert(List<Company> companies) {
		List<CompanyDTO> ret = new ArrayList<>();

		for (Company c : companies) {
			ret.add(convert(c));
		}

		return ret;
	}

}
