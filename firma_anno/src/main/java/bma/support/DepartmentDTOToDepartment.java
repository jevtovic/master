package bma.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bma.model.Department;
import bma.service.DepartmentService;
import bma.web.dto.DepartmentDTO;

@Component
public class DepartmentDTOToDepartment implements Converter<DepartmentDTO, Department> {

	@Autowired
	private DepartmentService departmentService;

	@Override
	public Department convert(DepartmentDTO source) {
		Department dept = null;

		if (source.getId() == null) {
			dept = new Department();

		} else {
			dept = departmentService.getById(source.getId());
		}
		dept.setName(source.getName());
		dept.setNoEmployees(source.getNoEmployees());

		return dept;
	}

}
