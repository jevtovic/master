package bma.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bma.model.Employee;
import bma.web.dto.EmployeeDTO;

@Component
public class EmployeeToEmployeeDTO implements Converter<Employee, EmployeeDTO> {

	@Override
	public EmployeeDTO convert(Employee source) {
		EmployeeDTO dto = new EmployeeDTO();

		dto.setId(source.getId());
		dto.setFullName(source.getFullName());
		dto.setPhone(source.getPhone());
		dto.setEmail(source.getEmail());
		dto.setSalary(source.getSalary());

		//

		return dto;
	}

	public List<EmployeeDTO> convert(List<Employee> employees) {
		List<EmployeeDTO> ret = new ArrayList<>();

		for (Employee e : employees) {
			ret.add(convert(e));
		}

		return ret;
	}

}
