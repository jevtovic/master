package bma.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import bma.model.Company;
import bma.service.CompanyService;
import bma.web.dto.CompanyDTO;

@Component
public class CompanyDTOToCompany implements Converter<CompanyDTO, Company> {

	@Autowired
	private CompanyService companyService;

	@Override
	public Company convert(CompanyDTO source) {
		Company comp = null;

		if (source.getId() == null) {
			comp = new Company();

		} else {
			comp = companyService.getById(source.getId());
		}
		comp.setName(source.getName());
		comp.setCountry(source.getCountry());
		comp.setCity(source.getCity());
		comp.setAddress(source.getAddress());

		return comp;
	}

}
