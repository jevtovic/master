package bma.repository;

import java.util.List;

import bma.model.Department;
import bma.model.Employee;

public interface DepartmentRepository {

	void delete(Long id);

	void deleteEmployeesByDepartment(Department dep);

	List<Department> findAll();

	Department findByName(String name);

	Department getById(Long id);

	void save(Department department);

	Integer totalNumberOfEmployeeByDepartment(Department dep);

	void update(Department dep);

//	void updateDepartmentNameByEmployee(String name, Employee emp);

	void updateEmployeeToDifferentDepartment(Department department, Department dept, Employee emp);

	void updateName(Long id, String name);

}