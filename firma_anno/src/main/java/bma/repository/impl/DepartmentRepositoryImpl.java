package bma.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import bma.dao.DepartmentDao;
import bma.model.Department;
import bma.model.Employee;
import bma.repository.DepartmentRepository;

@Repository("departmentRepository")
public class DepartmentRepositoryImpl implements DepartmentRepository {

	// private Long nextId = 0L;
	// private List<Department> departments = new ArrayList<>();
	//
	// List<Employee> employees = new ArrayList<>();
	//
	// Department department1 = new Department(nextId++, "R&D", 3L, employees);
	// {
	// departments.add(department1);
	// }
	//
	// Department department2 = new Department(nextId++, "IT", 4L);
	// {
	// departments.add(department2);
	// }
	//
	// Department department3 = new Department(nextId++, "Marketing", 2L);
	// {
	// departments.add(department3);
	// }

	@Autowired
	DepartmentDao departmentDao;

	@Override
	public void delete(Long id) {
		departmentDao.delete(id);

	}

	@Override
	public void deleteEmployeesByDepartment(Department dep) {
		departmentDao.deleteEmployeesByDepartment(dep);

	}

	@Override
	public List<Department> findAll() {

		return departmentDao.findAll();
	}

	@Override
	public Department findByName(String name) {
		return departmentDao.findByName(name);
	}

	@Override
	public Department getById(Long id) {
		return departmentDao.getById(id);
	}

	@Override
	public void save(Department department) {
		departmentDao.save(department);
	}

	@Override
	public Integer totalNumberOfEmployeeByDepartment(Department dep) {
		return departmentDao.totalNumberOfEmployeeByDepartment(dep);
	}

	@Override
	public void update(Department dep) {
		departmentDao.update(dep);
	}

//	@Override
//	public void updateDepartmentNameByEmployee(String name, Employee emp) {
//		departmentDao.updateName(id, name);
//
//	}

	@Override
	public void updateEmployeeToDifferentDepartment(Department department, Department dept, Employee emp) {
		departmentDao.updateEmployeeToDifferentDepartment(department, dept, emp);

	}

	@Override
	public void updateName(Long id, String name) {
		 departmentDao.updateName(id, name);
	}

}
