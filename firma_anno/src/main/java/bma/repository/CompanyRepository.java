package bma.repository;

import java.util.List;

import bma.model.Company;

public interface CompanyRepository {

	List<Company> findAll();

	void save(Company company);

	void delete(Long id);

	List<Company> getByCity(String city);
	
	Company getById(Long id);

	Company update(Company com);

}